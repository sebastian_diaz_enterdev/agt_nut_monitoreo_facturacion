Set WshShell = WScript.CreateObject("WScript.Shell")
Set fso = CreateObject("Scripting.FileSystemObject")

while true
	closeError()
	strFolder = getFolder()
	'Leer tiempo transcurrido
	content = ""
	on error resume next
	Set file = fso.OpenTextFile(strFolder & "\test.txt", 1)
	content = file.ReadAll
	minutos = (Time() -TimeValue(content))*24 * 60
	on error goto 0
	
	'Leer tiempo a esperar
	tiempoEspera = "5"
	Set fileTime = fso.OpenTextFile(strFolder & "\time.txt", 1)	
	tiempoEspera = fileTime.ReadAll
	tiempoEspera = CInt(tiempoEspera)
	'MsgBox "Content " & tiempoEspera & "minutos " & minutos
	if content <> "" then
		if abs(minutos) > tiempoEspera AND processIsRuninng("Menu_Agility") then
		  Set WshShell = WScript.CreateObject("WScript.Shell")
		  
		  command = """C:\Program Files\Agility\autos\AgilityRun3.3\AgilityRun3.3.exe"""
		  param = """C:\Program Files\Agility\autos\AGT_NUT_Monitoreo_Facturacion\AGT_NUT_Monitoreo_Facturacion.xml"""
		  
		  r1 = WshShell.Run("taskkill /F /IM AgilityRun3.3.exe", 1, true)
		  r2 = WshShell.Run("taskkill /F /IM agt_error.exe", 1, true)
		  if not processIsRuninng("AgilityRun3.3") then
			r3 = WshShell.Run(command & param, 1, true)
		  else
			r1 = WshShell.Run("taskkill /F /IM AgilityRun3.3.exe", 1, true)
			r3 = WshShell.Run(command & param, 1, true)
		  end if
		  
		  
		end if
	end if
	WScript.Sleep(60000)

Wend

Function processIsRuninng(FindProc)
	Set objWMIService = GetObject("winmgmts:" & "{impersonationLevel=impersonate}!\\.\root\cimv2")
	Set colProcessList = objWMIService.ExecQuery _
		("Select Name from Win32_Process WHERE Name LIKE '" & FindProc & "%'")
	processIsRuninng = False
	If colProcessList.count>0 then
		processIsRuninng = true
	End if
End Function 

Function getFolder()
	strPath = Wscript.ScriptFullName
	Set objFile = fso.GetFile(strPath)
	strFolder = fso.GetParentFolderName(objFile)
	getFolder = strFolder
End Function 

Function closeError()
	Set oShell = CreateObject("WScript.Shell")
	If oShell.AppActivate("Menu Agility") Then
		oShell.SendKeys "{ENTER}"
	End If
End Function